# 在termux上安装Yunzai-Bot

#### 介绍
轻松快速的在termux中安装云崽机器人

#### 安装教程

   ```
#先执行
gitee链接【国内推荐】:bash <(curl -L https://gitee.com/keduifu/install---yunzai-bot-on-termux/releases/download/Yunzai-Bot-termux/install.sh)
github链接【国外推荐】:bash <(curl -L https://github.com/keduifu/Yunzai-Bot-on-termux/releases/download/Yunzai-Bot/install.sh)
   ```
出现此界面时<br>
![输入图片说明](Screenshot_2023-03-19-16-59-42-39.jpg)<br>
输入:5 并回车 开始安装ubuntu【注意在新版中需要输入的应该是3】<br>
![输入图片说明](Screenshot_2023-03-19-17-05-04-79.jpg)<br>
输入:1 并回车 即可安装<br>
完成后 重新运行脚本<br>
输入:6 开启ubuntu【注意在新版中需要输入的应该是4】<br>
回到图一的界面之后 输入:1 开始安装云崽以及依赖<br>
安装完成后输入:2 开启云崽<br>
接下来按照流程配置即可
#### 警告
禁止修改脚本中的任何内容 否则出现问题不负任何责任
